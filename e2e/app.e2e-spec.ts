import { ReservationChatBotPage } from './app.po';

describe('reservation-chat-bot App', () => {
  let page: ReservationChatBotPage;

  beforeEach(() => {
    page = new ReservationChatBotPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
