import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// modules imports
import { ContactsListModule } from './pages/contacts-list/contacts-list.module';

// shared imports
import { ApiService } from './shared/services/api/api.service';

// components imports
import { AppComponent } from './app.component';
import { TabsComponent } from './shared/components/tabs/tabs.component';
import { ChatBoxComponent } from './shared/components/chat-box/chat-box.component';


@NgModule({
  declarations: [
    AppComponent,
    TabsComponent,
    ChatBoxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ContactsListModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
