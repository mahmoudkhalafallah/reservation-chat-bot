import { Injectable, OnInit } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService implements OnInit {
  baseUrl = '/assets/json/';
  constructor(private http: Http) { }

  ngOnInit() {

  }

  get(path: string, params?): Observable<any> {
    return this.http.get(`${this.baseUrl}${path}`, params )
    .map((res: Response) => res.json());
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${this.baseUrl}${path}`,
      JSON.stringify(body)
    )
    .catch(err => err.json())
    .map((res: Response) => res.json());
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${this.baseUrl}${path}`,
      JSON.stringify(body)
    )
    .catch(err => err.json())
    .map((res: Response) => res.json());
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${this.baseUrl}${path}`
    )
    .catch(err => err.json())
    .map((res: Response) => res.json());
  }

}
