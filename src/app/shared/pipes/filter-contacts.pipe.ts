import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterContacts'
})
export class FilterContactsPipe implements PipeTransform {

  transform(list: any, searchTerm?: any): any {
    // check if search term is here
    if ( searchTerm == undefined || list == undefined) { return list; }
    // filter the list according to the search term
    return list.filter((listItem) => {
      return listItem.name.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }

}
