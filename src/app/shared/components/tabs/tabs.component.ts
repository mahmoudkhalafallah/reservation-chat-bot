import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {
  @Output() navUpdated = new EventEmitter();
  activeTab = 'messages';

  constructor() { }

  ngOnInit() {
      this.navUpdated.emit(this.activeTab);
  }

  switchTab(tabName) {
    if (tabName !== this.activeTab) {
      switch (tabName) {
        case 'messages':
          this.activeTab = 'messages';
          break;
        case 'contacts':
          this.activeTab = 'contacts';
          break;
        case 'search':
          this.activeTab = 'search';
          break;
        default:
          this.activeTab = 'messages';
          break;
      }
      this.navUpdated.emit(this.activeTab);
    }
  }

}
