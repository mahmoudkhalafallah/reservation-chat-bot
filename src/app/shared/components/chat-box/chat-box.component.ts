import { Component, OnInit, Input, OnChanges, EventEmitter } from '@angular/core';
import { ApiService } from '../../services/api/api.service';

@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.css']
})
export class ChatBoxComponent implements OnInit {
  @Input() chatBoxData;
  back: EventEmitter<boolean> = new EventEmitter();
  message: string;
  chatHistory: Array<object>;
  userAgreed = false;
  botMessageCount = 1;
  fileLoaded = false;
  botMessages = [
    'Can i help you?',
    'Great, First things first.\n can you tell me more details about you?, lets start with your name and email address.',
    'Which Dates do you prefer to book on this month (enter the day number, hint: 12 not available)?',
    'Thank you, one of our staff will contact you soon.'
  ];
  errorMessage = [
    'This day is not available',
  ];
  constructor(private apiService: ApiService) { }

  ngOnInit() { }

  messageBuilder(message, byMe = true, time = new Date()) {
    return {
      message,
      time,
      byMe,
    };
  }

  send() {
    const storage = localStorage;

    if (this.chatBoxData.bot && this.message.length > 1) { // check if we are chatting the bot and there is a message submitted

      this.chatBoxData.chatHistory.push(this.messageBuilder(this.message)); // show the submitted message

      // check if the user is willing to interact
      const lowerCasedMessage = this.message.toLocaleLowerCase();
      if (lowerCasedMessage.includes('yes') || lowerCasedMessage.includes('y')) { this.userAgreed = true; }

      if (this.userAgreed && this.botMessageCount < this.botMessages.length) { // user agreed to interact

        // check if the date is available
        if (this.botMessageCount === 3 && Number(this.message) === 12) {
          this.chatBoxData.chatHistory.push(this.messageBuilder(this.errorMessage[0], false));
          this.botMessageCount--;
        }

        // respond to the user
        this.chatBoxData.chatHistory.push(this.messageBuilder(this.botMessages[this.botMessageCount], false));

        // store user answers
        storage.setItem(this.botMessages[this.botMessageCount - 1], this.message);


        this.botMessageCount++; // increment the count

      } else { // reached end of bot messages and restting
        this.chatBoxData.chatHistory.push(this.messageBuilder(this.botMessages[0], false));
        this.botMessageCount = 1;
        this.userAgreed = false;
      }
    } else if (this.message.length > 1) { // not chatting with the bot

      // check if we talked before or not
      if (this.chatBoxData.contact.lastMessage) {
        this.chatBoxData.chatHistory.push(this.messageBuilder(this.message));
      } else {
        this.chatBoxData.chatHistory = [this.messageBuilder(this.message)];
      }

      // updating last message
      this.chatBoxData.contact.lastMessage = this.messageBuilder(this.message);
    }

    // reseting input field
    this.message = '';
  }

  backClick() {
    this.back.emit(true);
  }

  fileChanged(e) {
    this.fileLoaded = e.target.value.length > 0 ? true : false;
  }

}
