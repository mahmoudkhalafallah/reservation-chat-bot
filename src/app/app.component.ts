import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactsListComponent } from './pages/contacts-list/contacts-list.component';
import { ChatBoxComponent } from './shared/components/chat-box/chat-box.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(ContactsListComponent) contactsList: ContactsListComponent;
  @ViewChild(ChatBoxComponent) chatBox: ChatBoxComponent;
  currentTab: string;
  chatBoxData = {
    chatBox: false
  };
  comingMessage;

  ngOnInit() {
   this.commingMessage();
   this.chatBox.back.subscribe( e => {
     this.chatBoxData = {
      chatBox: !e
    };
   });
  }

  commingMessage() {
    const Interval = setInterval( () => {
      this.comingMessage = {
        sender: {
          _id : '5984bb736dc77e1295e08ca6'
        },
        messageObj : {
          byMe: false,
          time: new Date(),
          message: 'fine and you ?',
          unread: true
        }
      };
      this.contactsList.updateMessages(this.comingMessage);
    }, 30000);
  }

  handleOpenChatBox(e) {
    this.chatBoxData = e;
  }

  handleNavUpdate(e) {
    this.currentTab = e;
  }

}
