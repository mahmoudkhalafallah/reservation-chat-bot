import { Component, OnInit, Input, Output, OnChanges, SimpleChange, ViewChild, AfterViewInit, EventEmitter, NgZone } from '@angular/core';
import { SearchBarComponent } from '../../pages/contacts-list/search-bar/search-bar.component';
import { ApiService } from '../../shared/services/api/api.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css'],
  providers: [ApiService]
})
export class ContactsListComponent implements OnInit, OnChanges {
  @Input() currentTab: string;
  @Output() openChatBox: EventEmitter<object> = new EventEmitter();
  @ViewChild(SearchBarComponent) searchbar: SearchBarComponent;
  contactList: Array<Object>;
  contact;
  searchTerm;
  newMessageBalloon;
  bot;
  clicked: boolean;
  constructor(private apiService: ApiService, private zone: NgZone) { }

  ngOnInit() {
    this.bot = {
      '_id': '5984bb730555c3910de584e9e',
      'index': 0,
      'guid': '89956fb1-c64d-4b7e-a9c0-1b692c5c9b2c',
      'isActive': true,
      'lastMessage': {
        'message': 'Can i help you?',
        'unread': true,
        'time': '1501033074000'
      },
      'picture': '../assets/img/Chat-Bot-Icon.svg',
      'name': 'Reservation Chat bot',
      'gender': 'female',
      'email': 'chatbot@reservation.com',
      'phone': '+1 (809) 480-2053',
      'address': '636 Langham Street, Bascom, Delaware, 3785',
      'about': 'Reservation Chat bot',
      'registered': '2015-10-02T03:55:25 -02:00',
      'tags': [
        'aliquip'
      ]
    };
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    this.get();
    this.searchTerm = '';
  }

  get() {
    this.apiService.get('contact_list.json').subscribe(res => {
      // check which tab is selected
      if (this.currentTab === 'messages') {
        // remove contacts with no conversations
        this.contactList = res.filter(contact => {
          return !!contact['lastMessage'];
        }).sort((a, b) => {
          return a.lastMessage.time > a.lastMessage.time;
        });

      } else {
        // sort contacts alphabetically
        this.contactList = res.sort((a, b) => {
          return a.name > b.name;
        });
      }
    }, err => {
      alert('Error: ' + err);
    });
  }

  handleSearch(searchTerm) {
    this.searchTerm = searchTerm;
  }

  updateMessages(newMessage) {
    if (this.currentTab === 'messages') {
      // find the contact with the new message and update the message
      const newContactMessage = this.contactList.find((contact, index) => {
        if (contact['_id'] === newMessage.sender._id) {
          contact['lastMessage'] = newMessage.messageObj;
          return true;
        }
      });
      // resort the list
      const updatedList = this.contactList.sort((a, b) => {
        if (a['lastMessage'].time < b['lastMessage'].time) {
          return 1;
        }
      });
      // hack for updating contacts list
      this.contactList = [];
      setTimeout(() => {
        this.contactList = updatedList;
      }, 0);
    } else {
      this.newMessageBalloon = true;
    }
  }

  openChat(contact, bot: boolean = false) {
    let chatHistoryPath;
    if (bot) {
      chatHistoryPath = 'chat_history-bot.json';
    } else {
      chatHistoryPath = 'chat_history.json';
    }
    this.apiService.get(chatHistoryPath, { id: contact._id }).subscribe(chatHistory => {
      this.openChatBox.emit({
        'chatBox': true,
        chatHistory,
        contact,
        bot
      });
    }, err => {
      alert('Error: ' + err);
    });
  }

}
