import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
  providers: [Subject]
})
export class SearchBarComponent implements OnInit {
  @Output() searchTerm: EventEmitter<string> = new EventEmitter();
  name;
  constructor( private subject: Subject<any>) { }

  ngOnInit() {
    // debounce while typing
    this.subject.debounceTime(200).subscribe( val => {
      this.searchTerm.emit(val);
    });
  }

  nameChanged(e) {
    this.subject.next(e.target.value);
  }

}
