import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ContactsListComponent } from './contacts-list.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FilterContactsPipe } from '../../shared/pipes/filter-contacts.pipe';
@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [ContactsListComponent, SearchBarComponent, FilterContactsPipe],
  exports: [ContactsListComponent],
})
export class ContactsListModule { }
